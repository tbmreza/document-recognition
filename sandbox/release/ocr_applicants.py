import argparse
import io
import json
import os
import tempfile
from google.cloud import vision
from pdf2image import convert_from_path
from validation_rules import validate


def google_ocr(filename, jpg_path):
    '''
    Returns texts read by Google OCR from jpg_path.
    Filename may end with '.pdf'
    '''    
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "My_Project_90491-6c27a2c82212.json"

    client = vision.ImageAnnotatorClient()

    with io.open(jpg_path, 'rb') as image_file:
        content = image_file.read()

    image = vision.types.Image(content=content)
    response = client.text_detection(image=image)
    texts = response.text_annotations
    
    # Handle reading image that doesn't contain text.
    try:
        texts = texts[0].description
    except IndexError:
        return filename+"\n"

    return filename+"\n"+texts


def what_type(file):
    '''
    Assuming properly named applicants' document files, return file's document type.
    '''
    doc_types = ('ktp','akte','ijazah','transkrip','toefl')
    found = 'unknown'

    for doc_type in doc_types:
        if doc_type in file:
            found = doc_type
            break

    return found


def standard_path(folder):
    '''
    Folder path string always ends with / and no / prefix.
    folder/
    '''
    try:
        if folder[0] == '/':
            folder = folder[1:]

        if folder.endswith('/'):
            return folder
        
        return folder+'/'
    
    # Handle empty string folder. 
    except IndexError:
        return folder

def write2json(a_dict,applicant_folder):
    '''
    Writes a dict object to applicant_folder/output/verdict.json
    '''
    applicant_folder = standard_path(applicant_folder)

    output_folder = applicant_folder+'output/'
    os.makedirs(output_folder, exist_ok=1)
    verdict_json_path = output_folder+'verdict.json'
    
    with open(verdict_json_path, 'w') as outfile:  
        json.dump(a_dict, outfile)

    return verdict_json_path


def process_documents(files,applicant_corpus):
    '''
    Run google_ocr() on JPG or PDF files.
    '''
    temp_jpg = ''

    for file in files:
        if file.endswith('.jpg'):
            applicant_corpus[what_type(file)] = google_ocr(filename=file, jpg_path=file)+'\n'

        elif file.endswith('.pdf'):                        
            with tempfile.TemporaryDirectory() as path:
                # file = 'path/to/file.pdf'
                images_from_path = convert_from_path(file, output_folder=path, last_page=1, first_page =0)

            save_dir = 'tmp'
            os.makedirs(save_dir, exist_ok=True)
            temp_jpg = save_dir+'/temp.jpg'

            for page in images_from_path:
                page.save(os.path.join(temp_jpg), 'JPEG')
        
            applicant_corpus[what_type(file)] = google_ocr(filename=file, jpg_path=temp_jpg)+'\n'

    if os.path.exists(temp_jpg):
        os.remove(temp_jpg)


def comprehend_folder(applicant_folder) -> list:
    '''
    Infer 
    `document_files: list`, and `input_json`: str 
    from applicant folder.
    '''
    applicant_folder = standard_path(applicant_folder)
    ls_applicant_folder = [applicant_folder+path for path in os.listdir(applicant_folder)]
    
    document_files = [path for path in ls_applicant_folder \
        if path.lower().endswith('.pdf') or path.lower().endswith('.jpg')]

    input_json = applicant_folder+'user_input.json'
    
    return document_files, input_json


def process_applicant(folder: 'peserta/applicant_ID/'):
    '''
    OCR documents in an applicant's folder.
    Run validation on `input_json` against read texts.
    Write verdict to json.
    '''
    applicant_corpus = {}

    document_files, input_json = comprehend_folder(folder)
    process_documents(document_files, applicant_corpus)
    
    verdict = validate(read_text=applicant_corpus,input_json=input_json)

    print(verdict)
    write2json(verdict,folder)


def set_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('input_folder', help='''Folder structure:                           
                                                id_peserta1/
                                                  (ktp|akte).(pdf|jpg)
                                                  ijazah.(pdf|jpg)
                                                  transkrip.(pdf|jpg)
                                                  toefl.(pdf|jpg)
                                                  user_input.json
                                                id_peserta2/
                                                ...''')    
    return parser.parse_args()


def execute_input_folder():
    ls_input_folder = [args.input_folder+path for path in os.listdir(args.input_folder)]
    for folder in ls_input_folder:
        process_applicant(folder)


# python3 ocr_applicants.py peserta/
if __name__ == '__main__':    
    args = set_args()
    execute_input_folder()
    