import os, sys; sys.path.insert(1, os.path.join(sys.path[0], '..'))

import unittest
from ocr_applicants import google_ocr, what_type, standard_path, write2json, process_documents, comprehend_folder, process_applicant

'''
Test only google_ocr, write2json, and comprehend_folder.

what_type, standard_path: would be an overkill to test
process_documents, process_applicant: covered in integration test
'''
class TestDocumentRecognition(unittest.TestCase):
    def test_google_ocr(self):
        filename = ''
        # Test different image file types.
        jpg_path='./units/google_ocr/sample.jpg'
        ocr_result = google_ocr(filename=filename, jpg_path=jpg_path)      
        self.assertTrue(len(ocr_result) > len(filename+'\n'))
                
        jpg_path='./units/google_ocr/sample.png'
        ocr_result = google_ocr(filename=filename, jpg_path=jpg_path)        
        self.assertTrue(len(ocr_result) > len(filename+'\n'))
        
        # Test image that doesn't contain text.
        jpg_path='./units/google_ocr/no_text.jpeg'
        ocr_result = google_ocr(filename=filename, jpg_path=jpg_path)
        self.assertTrue(len(ocr_result) == len(filename+'\n'))

    def test_write2json(self):
        applicant_folder = './units/comprehend_folder/arvieda/'
        # Test proper dict.
        proper_dict = \
        {
            'nama': {'valid': 'yes',
                    'message': 'OK'},
            'ipk': {'valid': 'no',
                    'message': 'ijazah=3.65 input=3.85'}
        }
        write2json(a_dict=proper_dict, applicant_folder=applicant_folder)

        # Test empty dict.
        empty_dict = {}
        write2json(a_dict=empty_dict, applicant_folder=applicant_folder)

        self.assertTrue(os.path.exists(applicant_folder+'user_input.json'))
    

    def test_comprehend_folder(self):
        print('test_comprehend_folder...')

        # Test applicant A folder
        folder_A = './units/comprehend_folder/arvieda'
        document_files, input_json = comprehend_folder(folder_A)
        print(f'document_files={document_files}')
        print(f'input_json={input_json}')
        self.assertTrue(type(document_files) == type([]))

        # Test applicant B folder
        folder_B = './units/comprehend_folder/mellysa/'
        document_files, input_json = comprehend_folder(folder_B)
        print(f'document_files={document_files}')
        print(f'input_json={input_json}')
        self.assertTrue(type(document_files) == type([]))

        print('test_comprehend_folder OK')
    
        


if __name__ == '__main__':
    unittest.main()